const keys=require("../token/keys")
const updateCheckList=require("../problems/9-update-checklist-simultaneously")

updateCheckList(true)
.then(()=>{
    console.log("checklist updated successfully")
})
.catch((error)=>{
    console.log(error)
})