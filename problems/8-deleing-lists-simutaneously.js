let getBoard = require("../problems/1-getBoard")
let getLists = require("../problems/3-getLists")
const keys = require("../token/keys")

function deletingLists() {
    return new Promise((resolve, reject) => {
        getBoard(keys, "66555a73b86c924f9e3010a3")
            .then((data) => {
                //console.log(data.id, "id")
              return   getLists(keys, data.id)
                    .then((data) => {
                        let deleteList = []
                        for (let cv of data) {
                            deleteList.push(fetch(`https://api.trello.com/1/lists/${cv.id}/closed?key=${keys.key}&token=${keys.token}&value=true`, {
                                method: 'PUT'
                            }))

                        }
                        return deleteList
                    })
               
            }).then((deleteList) => {
               resolve(Promise.all(deleteList))
            })
            .catch((error) => {
                reject(error)
            })

    })



}

module.exports=deletingLists