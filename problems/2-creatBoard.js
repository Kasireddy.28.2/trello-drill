//Create a function createBoard which takes the boardName as argument and returns a promise which resolves with newly created board data

function creatBoard(obj,name){
    return new Promise((resolve,reject)=>{
        fetch(`https://api.trello.com/1/boards/?name=${name}&key=${obj.key}&token=${obj.token}`, {
  method: 'POST'
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then((data) =>{
    resolve(data)
  } )
  .catch((err) =>{
    reject(err)
  } );
    })
}

module.exports=creatBoard