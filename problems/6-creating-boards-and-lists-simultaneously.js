const keys = require("../token/keys")
function creatBoard(obj, name) {
    return new Promise((resolve, reject) => {
        fetch(`https://api.trello.com/1/boards/?name=${name}&key=${obj.key}&token=${obj.token}`, {
            method: 'POST'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            })
            .then(text => {
                for (let index = 0; index < 3; index++) {
                    creatLists(keys,text.id,index).then(response => {
                        console.log(
                            `Response: ${response.status} ${response.statusText}`
                        );
                        return response.json();
                    })
                        .then(text => {
                            creatCard(keys, text.id).then((data) => {
                                resolve(data)
                            })
                        })
                    }
            })
            .catch(err => reject(err));
    })
}

//let newBoardId = "66554e71b177d8b5b8883531"

function creatLists(obj, boardId,index) {

    let listName = `list${index}`

    return fetch(`https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${obj.key}&token=${obj.token}`, {
        method: 'POST'
    })
}



//creatLists(keys,newBoardId)

function creatCard(keys, listId) {
    return fetch(`https://api.trello.com/1/cards?idList=${listId}&key=${keys.key}&token=${keys.token}`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json'
        }
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
        })
        .then(text => console.log(text))
        .catch(err => console.error(err));
}

module.exports=creatBoard
