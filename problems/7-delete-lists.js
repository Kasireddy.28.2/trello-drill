let getBoard=require("../problems/1-getBoard")
let getLists=require("../problems/3-getLists")
const keys=require("../token/keys")

function listIds(){
    return new Promise((resolve,reject)=>{
        getBoard(keys,"66555a73b86c924f9e3010a3")
    .then((data)=>{
     
        console.log(data.id)
        getLists(keys,data.id)
        .then((data)=>{
            console.log(data)
            data.forEach((cv)=>{
               deleteLists(keys,cv.id)

            })
        })
    }).then(()=>{
        resolve("successfully")
    })
    .catch((error)=>{
        reject(error)
    })

    })
    


}

function deleteLists(keys,id){
    fetch(`https://api.trello.com/1/lists/${id}/closed?key=${keys.key}&token=${keys.token}&value=true`, {
  method: 'PUT'
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.text();
  })
  .then(text => console.log(text))
  .catch(err => console.error(err));

}

module.exports=listIds