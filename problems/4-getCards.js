// Create a function getCards which takes a listId as argument and returns a promise which resolves with cards data

function getCards(obj,id){
    return new Promise((resolve,reject)=>{
        fetch(`https://api.trello.com/1/lists/${id}/cards?key=${obj.key}&token=${obj.token}`, {
  method: 'GET',
  headers: {
    'Accept': 'application/json'
  }
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(text => resolve(text))
  .catch(err => reject(err));
    })
        
}

module.exports=getCards