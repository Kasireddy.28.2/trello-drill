//Create a function getLists which takes a boardId as argument and returns a promise which resolved with lists data


function getLists(obj,id){
    return new Promise((resolve,reject)=>{
        fetch(`https://api.trello.com/1/boards/${id}/lists?key=${obj.key}&token=${obj.token}`, {
  method: 'GET',
  headers: {
    'Accept': 'application/json'
  }
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );

    return response.json();
  })
  .then(data => {
    resolve(data)
  })
  .catch(err => reject(err));
        
})
}

module.exports=getLists