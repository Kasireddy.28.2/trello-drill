const keys = require("../token/keys");

function updateCheckList(value) {
    return new Promise((resolve, reject) => {
        if (typeof value === "boolean") {

            fetch(`https://api.trello.com/1/boards/66555a73b86c924f9e3010a3/checklists?key=${keys.key}&token=${keys.token}`, {
                method: 'GET'
            })
                .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    );

                    return response.json();
                })
                .then((data) => {
                    data.forEach(element => {
                        let cardId = element.idCard
                        element['checkItems'].forEach((item) => {
                            console.log(item)
                            let checkListId = item.idChecklist
                            let id = item.id
                            fetch(`https://api.trello.com/1/cards/${cardId}/checklist/${checkListId}/checkItem/${id}/state?key=${keys.key}&token=${keys.token}&value=${value} `,
                                {
                                    method: 'PUT'
                                })
                                .then(response => {
                                    console.log(
                                        `Response: ${response.status} ${response.statusText}`
                                    );
                                    return response.text();
                                })
                                .then((data) =>
                                    console.log(data)
                                )
                                .catch((error) => {
                                    console.error(error)
                                });
                        })
                    })
                }
                ).then(() => {
                    resolve('success')
                })
                .catch((error) => {
                    reject(error)
                });
        } else {
            reject(error)
        }

    })
}


module.exports = updateCheckList
