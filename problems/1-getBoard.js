//Create a function getBoard which takes the boardId as argument and returns a promise which resolves with board data


function getBoard(obj,id){
    return new Promise((resolve,reject)=>{
        fetch(`https://api.trello.com/1/boards/${id}?key=${obj.key}&token=${obj.token}`, {
  method: 'GET',
  
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json()
  })
  .then(text =>{
    resolve(text)
})
  .catch((err) => {
    reject(err)
  })

    })

    
}

module.exports=getBoard