let getLists = require("../problems/3-getLists")
let getCards = require("../problems/4-getCards")
let keys = require("../token/keys")

function getAllCards(boardId) {
    return new Promise((resolve, reject) => {
        let listIds=[]
        getLists(keys, boardId)
            .then((listData) => {
                
                for(let list of listData){
                    console.log(list)
                    listIds.push(getCards(keys,list.id))
                }
                return listIds
            }).then((promises)=>{
                return Promise.all(promises)
            }).then((data)=>{
                resolve(data)
            }).catch((error)=>{
                reject(error)
            })
    })
}

module.exports=getAllCards